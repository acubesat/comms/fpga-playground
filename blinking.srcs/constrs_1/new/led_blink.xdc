## LED
set_property -dict { PACKAGE_PIN R14 IOSTANDARD LVCMOS33 } [get_ports { blink0 }];
set_property -dict { PACKAGE_PIN P14 IOSTANDARD LVCMOS33 } [get_ports { blink1 }];
set_property -dict { PACKAGE_PIN N16 IOSTANDARD LVCMOS33 } [get_ports { blink2 }];
set_property -dict { PACKAGE_PIN M14 IOSTANDARD LVCMOS33 } [get_ports { blink3 }];

## SWITCH
set_property -dict { PACKAGE_PIN D19 IOSTANDARD LVCMOS33 } [get_ports { switch0 }];
set_property -dict { PACKAGE_PIN D20 IOSTANDARD LVCMOS33 } [get_ports { switch1 }];
set_property -dict { PACKAGE_PIN L20 IOSTANDARD LVCMOS33 } [get_ports { switch2 }];
set_property -dict { PACKAGE_PIN L19 IOSTANDARD LVCMOS33 } [get_ports { switch3 }];