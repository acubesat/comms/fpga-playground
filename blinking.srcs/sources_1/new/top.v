`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 21.10.2022 20:44:55
// Design Name: 
// Module Name: top
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module top(
        input switch0,
        input switch1,
        input switch2,
        input switch3,
        output blink0,
        output blink1,
        output blink2,
        output blink3
    );
    
    assign blink0 = switch0;
    assign blink1 = switch1;
    assign blink2 = switch2;
    assign blink3 = switch3;
    
endmodule
